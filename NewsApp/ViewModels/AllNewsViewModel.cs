﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using NewsApp.Messages;
using NewsApp.Models;
using NewsApp.Navigation;
using NewsApp.Services.StorageService;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NewsApp.ViewModels
{
    class AllNewsViewModel : ViewModelBase
    {
        #region Fields and Properties

        public ObservableCollection<NewsItem> News { get => new ObservableCollection<NewsItem>(storage.News); }

        private NewsItem selectedNewsItem;
        public NewsItem SelectedNewsItem
        {
            get => selectedNewsItem;
            set => Set(ref selectedNewsItem, value);
        }

        #endregion


        #region Dependencies

        IStorage storage;
        INavigationService navigationService;

        #endregion

        #region Messages

        private AddNewsItemViewModelInitializationMessage AddNewsItemViewModelInitializationMessage = new AddNewsItemViewModelInitializationMessage();

        private NewsItemDetailViewWrapperMessage NewsItemDetailViewWrapperMessage = new NewsItemDetailViewWrapperMessage();

        private EditNewsItemWrapperMessage EditNewsItemWrapperMessage = new EditNewsItemWrapperMessage();

        #endregion

        #region Ctor

        public AllNewsViewModel(INavigationService navigationService, IStorage storage)
        {
            this.storage = storage;
            this.navigationService = navigationService;
        }

        #endregion


        #region Commands

        private RelayCommand addNewsItemCommand;

        public RelayCommand AddNewsItemCommand
        {
            get => addNewsItemCommand ?? (addNewsItemCommand = new RelayCommand(
                    () =>
                    {
                        Messenger.Default.Send<AddNewsItemViewModelInitializationMessage>(AddNewsItemViewModelInitializationMessage);
                        navigationService.NavigateTo<AddNewsItemViewModel>();
                    }
                ));
        }


        private RelayCommand<NewsItem> showDetailedNewsItemCommand;
        public RelayCommand<NewsItem> ShowDetailedNewsItemCommand
        {
            get => showDetailedNewsItemCommand ?? (showDetailedNewsItemCommand = new RelayCommand<NewsItem>(
                    n =>
                    {
                        Messenger.Default.Send<NewsItemDetailViewWrapperMessage>(NewsItemDetailViewWrapperMessage.WrapMessage(n));
                        navigationService.NavigateTo<NewsItemDetailViewModel>();


                    }
                ));
        }

        private RelayCommand<NewsItem> deleteNewsItemCommand;
        public RelayCommand<NewsItem> DeleteNewsItemCommand
        {
            get => deleteNewsItemCommand ?? (deleteNewsItemCommand = new RelayCommand<NewsItem>(
                n =>
                {
                    try
                    {
                        storage.DeleteNewsItem(n);
                        RaisePropertyChanged(nameof(News));
                    }
                    catch (ArgumentNullException ex)
                    {
                        MessageBox.Show("NewsItem was not found");
                    }
                }
            ));
        }


        private RelayCommand<NewsItem> editNewsItemCommand;
        public RelayCommand<NewsItem> EditNewsItemCommand
        {
            get => editNewsItemCommand ?? (editNewsItemCommand = new RelayCommand<NewsItem>(
                    n =>
                    {
                        Messenger.Default.Send<EditNewsItemWrapperMessage>(EditNewsItemWrapperMessage.WrapMessage(n));
                        navigationService.NavigateTo<EditNewsItemViewModel>();


                    }
                ));
        }




        #endregion
    }
}
