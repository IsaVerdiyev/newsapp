﻿
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;
using NewsApp.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsApp.ViewModels
{
    class AppViewModel: ViewModelBase
    {
        #region Properties
        ViewModelBase currentPage;

        public ViewModelBase CurrentPage
        {
            get => currentPage;
            set => Set(ref currentPage, value);
        }
        #endregion

        #region Dependencies
        INavigationService navigationService;
        #endregion 

        #region Consturctor
        public AppViewModel(INavigationService navigationService)
        {
            this.navigationService = navigationService;

            Messenger.Default.Register<ViewModelBase>(this, param => CurrentPage = param);
        }
        #endregion
    }
}
