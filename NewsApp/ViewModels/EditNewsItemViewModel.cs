﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using NewsApp.Messages;
using NewsApp.Models;
using NewsApp.Navigation;
using NewsApp.Services.StorageService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsApp.ViewModels
{
    class EditNewsItemViewModel: ViewModelBase
    {
        #region Fields and Properties

        private NewsItem newsItem;

        private string header;
        public string Header {
            get => header;
            set
            {
                Set(ref header, value);
                EditNewsItemCommand.RaiseCanExecuteChanged();
            }
        }

        private string text;
        public string Text {
            get => text;
            set
            {
                Set(ref text, value);
                EditNewsItemCommand.RaiseCanExecuteChanged();
            }
        }
        
        #endregion


        #region Dependencies


        INavigationService navigationService;
        IStorage storage;

        #endregion



        #region Ctor

        public EditNewsItemViewModel(INavigationService navigationService, IStorage storage)
        {
            this.navigationService = navigationService;
            this.storage = storage;
            Messenger.Default.Register<EditNewsItemWrapperMessage>(this, m =>
            {
                newsItem = m.NewsItem;
                Header = newsItem.Header;
                Text = newsItem.Text;
            });
        }

        #endregion


        #region Commands

        private RelayCommand editNewsItemCommand;
        public RelayCommand EditNewsItemCommand
        {
            get => editNewsItemCommand ?? (editNewsItemCommand = new RelayCommand(
                () => {
                    newsItem.Header = Header;
                    newsItem.Text = Text;
                    storage.UpdateNewsItem(newsItem);
                    navigationService.NavigateTo<AllNewsViewModel>();
                }
                ,() => !string.IsNullOrWhiteSpace(Header) && !string.IsNullOrWhiteSpace(Text)
            ));
        }


        private RelayCommand cancelEditingCommand;
        public RelayCommand CancelEditingCommand
        {
            get => cancelEditingCommand ?? (cancelEditingCommand = new RelayCommand(
                () => navigationService.NavigateTo<AllNewsViewModel>()
            ));
        }

        #endregion
    }
}
