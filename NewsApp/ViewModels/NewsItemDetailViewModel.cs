﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using NewsApp.Messages;
using NewsApp.Models;
using NewsApp.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsApp.ViewModels
{
    class NewsItemDetailViewModel : ViewModelBase
    {
        #region Fields and Properties

        private NewsItem newsItem;
        public NewsItem NewsItem { get => newsItem; set => Set(ref newsItem, value); }
        #endregion


        #region Dependencies

        
        INavigationService navigationService;

        #endregion



        #region Ctor

        public NewsItemDetailViewModel(INavigationService navigationService)
        {
            NewsItem = new NewsItem();
            this.navigationService = navigationService;
            Messenger.Default.Register<NewsItemDetailViewWrapperMessage>(this, n =>
            {
                NewsItem = n.NewsItem;
            });

            
        }

        #endregion


        #region Commands

        private RelayCommand goBackCommand;
        public RelayCommand GoBackCommand {
            get => goBackCommand ?? (goBackCommand = new RelayCommand(
                () => navigationService.NavigateTo<AllNewsViewModel>()
            ));
        }

        #endregion
    }
}
