﻿
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using NewsApp.Messages;
using NewsApp.Navigation;
using NewsApp.Services.StorageService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsApp.ViewModels
{
    class AddNewsItemViewModel : ViewModelBase
    {
        #region Fields and Properties

        private string header;
        public string Header {
            get => header;
            set
            {
                Set(ref header, value);
                AddNewsItemCommand.RaiseCanExecuteChanged();
            }
        }

        private string text;
        public string Text {
            get => text;
            set
            {
                Set(ref text, value);
                AddNewsItemCommand.RaiseCanExecuteChanged();
            }
        }


        #endregion


        #region Dependencies


        INavigationService navigationService;

        IStorage storage;

        #endregion



        #region Ctor

        public AddNewsItemViewModel(INavigationService navigationService, IStorage storage)
        {
            this.navigationService = navigationService;
            this.storage = storage;
            Messenger.Default.Register<AddNewsItemViewModelInitializationMessage>(this, m =>
            {
                Header = "";
                Text = "";
            });
        }

        #endregion


        #region Commands

        private RelayCommand addNewsItemCommand;

        public RelayCommand AddNewsItemCommand
        {
            get => addNewsItemCommand ?? (addNewsItemCommand = new RelayCommand(
                    () =>
                    {
                        storage.AddNewsItem(new Models.NewsItem { Header = Header, Text = Text });
                        navigationService.NavigateTo<AllNewsViewModel>();
                    }
                    , () => !string.IsNullOrWhiteSpace(Header) && !string.IsNullOrWhiteSpace(Text)
                ));
        }

        private RelayCommand cancelAddingNewsItemCommand;
        public RelayCommand CancelAddingNewsItemCommand
        {
            get => cancelAddingNewsItemCommand ?? (cancelAddingNewsItemCommand = new RelayCommand(
                () => navigationService.NavigateTo<AllNewsViewModel>()
            ));
        }

        #endregion
    }
}
