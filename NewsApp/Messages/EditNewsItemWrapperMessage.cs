﻿using NewsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsApp.Messages
{
    class EditNewsItemWrapperMessage
    {
        public NewsItem NewsItem { get; set; }


        public EditNewsItemWrapperMessage WrapMessage(NewsItem newsItem)
        {
            NewsItem = newsItem;
            return this;
        }
    }
}
