﻿using NewsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsApp.Messages
{
    class NewsItemDetailViewWrapperMessage
    {
        public NewsItem NewsItem { get; set; }

        public NewsItemDetailViewWrapperMessage WrapMessage(NewsItem newsItem)
        {
            NewsItem = newsItem;
            return this;
        }
    }
}
