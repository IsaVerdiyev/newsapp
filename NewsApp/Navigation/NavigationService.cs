﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Messaging;

namespace NewsApp.Navigation
{
    class NavigationService : INavigationService
    {
        private Dictionary<Type, ViewModelBase> viewmodels = new Dictionary<Type, ViewModelBase>();

        public void Register<T>(ViewModelBase viewModel)
        {
            viewmodels[typeof(T)] = viewModel;
        }

        public void NavigateTo<T>() where T : ViewModelBase
        {
            try
            {
                Messenger.Default.Send(viewmodels[typeof(T)]);
            }
            catch (Exception ex)
            {
                throw new PageNotFoundException($"Page {typeof(T).ToString()} not found. Error occured in NavigateTo function");
            }
        }

        public void NavigateTo(Type t)
        {
            try
            {
                Messenger.Default.Send(viewmodels[t]);
            }
            catch (Exception ex)
            {
                throw new PageNotFoundException($"Page {t} not found. Error occured in NavigateTo function");
            }
        }


      
    }
}
