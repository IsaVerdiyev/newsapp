﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsApp.Navigation
{
    interface INavigationService
    {
        void Register<T>(ViewModelBase viewModel);
        void NavigateTo<T>() where T: ViewModelBase;
        void NavigateTo(Type t); 
    }
}
