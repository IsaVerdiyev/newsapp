﻿using NewsApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsApp.Services.StorageService
{
    interface IStorage
    {
        List<NewsItem> News { get; }

        void AddNewsItem(NewsItem newsItem);

        void DeleteNewsItem(NewsItem newsItem);

        void UpdateNewsItem(NewsItem newsItem);
    }
}
