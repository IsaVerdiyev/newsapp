﻿using NewsApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsApp.Services.StorageService
{
    class NewsContext: DbContext
    {
        

        public virtual DbSet<NewsItem> NewsItems { get; set; }

        public NewsContext():base("DBConnection")
        {

        }

       
    }
}
