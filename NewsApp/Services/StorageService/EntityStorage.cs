﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NewsApp.Models;

namespace NewsApp.Services.StorageService
{
    class EntityStorage : IStorage
    {

        object lockObject = new object();
        

        public List<NewsItem> News
        {
            get
            {
                lock (lockObject)
                {
                    using(var context = new NewsContext())
                    {
                        return context.NewsItems.ToList();
                    }
                }
            }
        }

        public void AddNewsItem(NewsItem newsItem)
        {
            lock (lockObject)
            {
                using (var context = new NewsContext())
                {
                    context.NewsItems.Add(newsItem);
                    context.SaveChanges();
                }
            }

        }

        public void DeleteNewsItem(NewsItem newsItem)
        {
            lock (lockObject)
            {
                using (var context = new NewsContext())
                {
                    context.NewsItems.Remove(context.NewsItems.Find(newsItem.Id));
                    context.SaveChanges();
                }
            }
        }

        public void UpdateNewsItem(NewsItem newsItem)
        {
            lock (lockObject)
            {
                using (var context = new NewsContext())
                {
                    context.NewsItems.Find(newsItem.Id).Header = newsItem.Header;
                    context.NewsItems.Find(newsItem.Id).Text = newsItem.Text;
                    context.SaveChanges();
                }
            }
        }
    }
}
