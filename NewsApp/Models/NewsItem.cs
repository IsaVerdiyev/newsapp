﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewsApp.Models
{
    class NewsItem: ObservableObject
    {
        #region Fields and Properties

        public int Id { get; set; }

        string header;
        public string Header { get => header; set => Set(ref header, value); }

        string text;
        public string Text { get => text; set => Set(ref text, value); }


        #endregion

        public override string ToString()
        {
            return Header;
        }
    }
}
