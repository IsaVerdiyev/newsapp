﻿using Autofac;
using Autofac.Configuration;
using Microsoft.Extensions.Configuration;
using NewsApp.Navigation;
using NewsApp.Services.StorageService;
using NewsApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NewsApp.Tools
{
    class ViewModelLocator
    {
        INavigationService navigationService = new NavigationService();
        IStorage storage = new EntityStorage();
       
        public AppViewModel AppViewModel { get; }

        public AllNewsViewModel AllNewsViewModel { get; }

        public AddNewsItemViewModel AddNewsItemViewModel { get; }

        public NewsItemDetailViewModel NewsItemDetailViewModel { get; }

        public EditNewsItemViewModel EditNewsItemViewModel { get; }
       

        public ViewModelLocator()
        {
            try
            {
                var config = new ConfigurationBuilder();
                config.AddJsonFile("autofac.json");
                var module = new ConfigurationModule(config.Build());
                var builder = new ContainerBuilder();
                builder.RegisterModule(module);
                builder.RegisterInstance(navigationService).As<INavigationService>().SingleInstance();
                builder.RegisterInstance(storage).As<IStorage>().SingleInstance();
              
                var container = builder.Build();

                using (var scope = container.BeginLifetimeScope())
                {
                    AppViewModel = scope.Resolve<AppViewModel>();
                    AllNewsViewModel = scope.Resolve<AllNewsViewModel>();
                    AddNewsItemViewModel = scope.Resolve<AddNewsItemViewModel>();
                    NewsItemDetailViewModel = scope.Resolve<NewsItemDetailViewModel>();
                    EditNewsItemViewModel = scope.Resolve<EditNewsItemViewModel>();
                }


                navigationService.Register<AllNewsViewModel>(AllNewsViewModel);
                navigationService.Register<AddNewsItemViewModel>(AddNewsItemViewModel);
                navigationService.Register<NewsItemDetailViewModel>(NewsItemDetailViewModel);
                navigationService.Register<EditNewsItemViewModel>(EditNewsItemViewModel);

                navigationService.NavigateTo<AllNewsViewModel>();
                

            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
